var express = require('express');
var router = express.Router();
var Quagga = require('quagga');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('Startseite', { title: 'Test' });
});

module.exports = router;
