Shotverteilungsgenerator - 04.05.2018 - Version 1.0

Die Anwendung soll dazu dienen, aus einer Liste von Alkohol einen zufälligen 
Shot auszuwählen.




Beschreibung: 
Die Getränke können eingescannt und angezeigt werden über einen Webbrowser. 
Anhand der Etiketten soll die OCR-Lösung erkennen, um welches Getränk es 
sich handelt. Darauf basierend kann ein zufälliges Getränk generiert werden. 



Installationsanweisungen: 

- Ausführen des Dockerfiles mit Docker
- Kamera erforderlich (mit Autofokus) 
- funktioniert in: 
	* Edge ab Version 16
	* Safari ab Version 11.1
	* Opera ab Version 52.0 
	* Chrome ab Version 68
	* Firefox ab Version 58



Kontakt: 
E-Mail: trinkspiel@gmx.net
