var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GetränkSchema = new Schema({
    name: {
        required: true,
        type: String
    },
    ean: {
        required: true,
        type: Number,
        unique: true
    },
    vol: {
        type: Number,
        required: true
        
    },
    
});

var Getränk = mongoose.model('Getränk', GetränkSchema);

module.exports = Getränk;