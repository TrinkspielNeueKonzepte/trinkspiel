FROM dmitry7887/alpine-node-git
 

#Create app directory


RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app



#Install app dependencies


RUN git clone https://gitlab.com/TrinkspielNeueKonzepte/trinkspiel.git /usr/src/app


RUN npm install


#Bundle app source


COPY . /usr/src/app



EXPOSE 815
CMD [ "npm", "start" ]